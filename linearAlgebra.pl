% Create Vector
vectorCreate(N, [0|T]) :-
    N > 0,
    N2 is N-1,
    vectorCreate(N2, T).
vectorCreate(0, []).

% Create Matrix
matrixCreate(N, M, [H|T]) :-
    N > 0,
    N2 is N-1,
    vectorCreate(M, H),
    matrixCreate(N2, M, T).
matrixCreate(0, M, [X]) :-
    vectorCreate(M, X).

% Set an element in matrix
matrixSet(0, Value, [H|T]) :-
    H is Value.


% Vector Multiply
vectorMultiply([X], [Y], [R]):- R is X*Y.
vectorMultiply([H1|T1], [H2|T2], [H3|T3]):- 
    vectorMultiply(T1, T2, T3),
    H3 is H1 * H2.
